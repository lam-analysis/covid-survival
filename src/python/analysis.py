## LOAD MODULES

import pandas as pd
import numpy  as np

from sklearn.model_selection import train_test_split
from sksurv.datasets import get_x_y

from sksurv.ensemble import (
    ComponentwiseGradientBoostingSurvivalAnalysis as CGB,
    GradientBoostingSurvivalAnalysis as GB,
    RandomSurvivalForest as RF,
    ExtraSurvivalTrees as extTree
)

from sksurv.linear_model import CoxPHSurvivalAnalysis as Cox
from sksurv.tree         import SurvivalTree

from sksurv.metrics import (
    cumulative_dynamic_auc,
    integrated_brier_score
)


## FUNC

def mkevent(event, time):
    return(np.array(
        [(i, j) for i, j in zip(event, time)],
        dtype = [("Status", "?"), ("Survival_in_days", "<f8")]
    ))


## PREP

# Set seed
seed = 1810

# Read data frame
tbl = pd.read_csv("data/processed//survival.csv")

# Transform categorical variable into numeric
tbl.Vasoaktif.replace(
    ["tidak menggunakan", "menggunakan"], [0, 1], inplace=True
)

# Transform all values into numeric 
tbl = tbl.apply(pd.to_numeric)

# Evaluate the NaN
tbl.info()
tbl.isna().sum()


## FEAT

# Set the name of event to evaluate
event = "mortality"

# Selecting predictors and outcome
x = tbl.iloc[:, np.invert(tbl.columns.isin(["mortality", "LOS", "ventilator"]))]
y = mkevent(tbl[event], tbl.LOS)

# Set event times
lo, up = np.percentile(tbl.LOS, [10, 90])
ytimes = np.arange(lo, up + 1, 1e-2)

# Splitting into training and testing dataset
xtrain, xtest, ytrain, ytest = train_test_split(
    x, y, test_size=0.3, random_state=seed
)


## MODEL

# Dictionary of estimators
estimators = {
    "CGB"      : CGB(),
    "GB"       : GB(),
    "RF"       : RF(),
    "Ext Tree" : extTree(),
    "Cox"      : Cox(),
    "Tree"     : SurvivalTree()
}

# Containers for fitted models and its metrics
models  = {}
aucs    = {}
probs   = {}
metrics = {"Estimator":[], "C-Index":[], "IBS":[], "AUC":[]}

# Iterate all estimators then compute the metrics
for k, v in estimators.items():
    # Fitting the model
    mod = v
    mod.fit(xtrain, ytrain)

    # Calculate probability
    prob = np.row_stack([
        f(ytimes) for f in mod.predict_survival_function(xtest)
    ])

    # Calculate the risk score and AUC
    risk = mod.predict(xtest)
    auc, mean_auc = cumulative_dynamic_auc(ytrain, ytest, risk, ytimes)

    # Calculate the integrated Brier score
    IBS = integrated_brier_score(y, ytest, prob, ytimes)

    # Update all containers
    models.update({k:mod})
    probs.update({k:prob})
    aucs.update({k:auc})
    metrics["Estimator"].append(k)
    metrics["C-Index"].append(mod.score(xtest, ytest))
    metrics["IBS"].append(IBS)
    metrics["AUC"].append(mean_auc)

# Evaluate model metrics
pd.DataFrame(metrics)
