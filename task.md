Updating models:

See the potentials of incorporating GLM into the current best model

Missing value imputation:

1. NE dan dobutamin sementara dapat di-remove dulu
2. Gunakan Vasoaktif sbg representasi dari NE/Dobu
3. Missing value pada var pemeriksaan akan diprediksi dengan intermediary model

Pemodelan:

1. Lakukan pemodelan menggunakan data yg komplit
2. Lakukan pemodelan menggunakan data dengan imputasi
3. Bandingan kedua approach untuk mendapatkan parsimonious model

Jenis model:

1. Linear model dengan menggunakan Cox regression
2. Ensemble model:
  - Gradient boosting survival model
  - Random fores survival model
  - Component-wise gradient boosting
3. Survival tree
4. Support vector machine survival model

User requirement pembuatan aplikasi:

1. 
